<?php

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\ManageOrganizationCreateRequest $request
     */
    public function store(ManageOrganizationCreateRequest $request)
    {
    	/* @var $service ManageOrganizationService */
    	try {
    		DB::beginTransaction();

    		$attributes = $request->except(['_token', 'password_confirmation']);

    		$dataMail = [
    			'email' => $attributes['email'],
    			'login' => $attributes['login'],
    			'password' => $attributes['password'],
    			'name' => $attributes['name'],
    		];

    		$attributes['password'] = Hash::make($attributes['password']);
    		$attributes['role_id'] = Role::getRoleIdByName(Role::SIMPLE_MANAGE_ORG_ROLE_NAME);
    		$attributes['start_at'] = date('Y-m-d', strtotime(str_replace('/', '-', $attributes['start_at'])));

    		$manageOrganizationCollectionObject = $this->service->createManageOrganization($attributes);

    		$employeeJurisdictionToOrgIds = [];
    		if (isset($attributes['employee_jurisdiction_ids']) && is_array($attributes['employee_jurisdiction_ids'])) {
    			$serviceToLinkedTable = resolve(EmployeeService::class);
    			$employeeJurisdictionOriginalIds = $attributes['employee_jurisdiction_ids'];
    			foreach ($employeeJurisdictionOriginalIds as $employeeJurisdictionOriginalId) {
    				$employeeJurisdictionToOrg = $serviceToLinkedTable->createEmployeeJurisdictionToOrganization([
    					'manage_organization_id' => $manageOrganizationCollectionObject->id,
    					'jurisdiction_id' => (int)$employeeJurisdictionOriginalId,
    				]);
    				array_push($employeeJurisdictionToOrgIds, $employeeJurisdictionToOrg->id);
    			}
    		}

    		$attributes['employee_jurisdiction_ids'] = $employeeJurisdictionToOrgIds;
    		$manageOrganizationModel = ManageOrganization::find($manageOrganizationCollectionObject->id);
    		$this->service->updateManageOrganization($manageOrganizationModel, $attributes);

    		DB::commit();

    		Mail::send('emails.manage-organization.registration', compact('dataMail'), function ($message) use ($dataMail) {
    			$message->to($dataMail['email'])->subject('Company successfully registration');
    		});

    		Session::put('success', 'Organization Saved.');
            //todo realize after routes created

    		return redirect(route('manage-organization.index', $manageOrganizationModel->getKey()));
    	} catch (\Exception $e) {
    		DB::rollBack();

    		Session::put('error', $e->getMessage());

    		return redirect(route('manage-organization.index'));
            //return redirect(route('manage-organization.index'))->withErrors($e->getMessage());
    	}
    }